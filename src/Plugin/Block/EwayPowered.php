<?php
/**
 * @file
 * Contains \Drupal\uc_eway\Plugin\Block\EwayPowered.
 */

namespace Drupal\uc_eway\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a 'EwayPowered' block.
 *
 * @Block(
 *   id = "eway_powered_block",
 *   admin_label = @Translation("Powered by eWAY")
 * )
 */
class EwayPowered extends BlockBase {

  /**
  * {@inheritdoc}
  */
  // $form = \Drupal::formBuilder()->getForm('Drupal\demo\Form\DemoForm');
  public function build() {
    global $base_url;
    $config = $this->getConfiguration();

    //$size = \Drupal::config('uc_eway.settings')->get('uc_eway_block_powered_by_size');
    $size = isset( $config['uc_eway_logo_size'] ) ? $config['uc_eway_logo_size'] : 's';
    //$colour = \Drupal::config('uc_eway.settings')->get('uc_eway_block_powered_by_colour');
    $colour = isset( $config['uc_eway_logo_colour'] ) ? $config['uc_eway_logo_colour'] : 'c';
    if ($size == 'large') $size = 'medium';
    return array(
      '#markup' => '<img src="'. $base_url .'/'. drupal_get_path('module', 'uc_eway') . '/images/' . ucfirst($size) . ucfirst($colour) . '.gif" />',
    );
  }

  /**
  * {@inheritdoc}
  */
  public function blockForm( $form, FormStateInterface $form_state ) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $form['uc_eway_logo_size'] = array(
      '#type' => 'select',
      '#title' => 'Logo size',
      '#options' => array(
        'large' => 'Large',
        'small' => 'Small',
      ),
      '#default_value' => $config['uc_eway_logo_size'],
    );
    $form['uc_eway_logo_colour'] = array(
      '#type' => 'select',
      '#title' => 'Logo colour',
      '#options' => array(
        'grey' => 'Grey',
        'white' => 'White',
      ),
      '#default_value' => $config['uc_eway_logo_colour'],
    );
    return $form;
  }

  public function blockSubmit( $form, FormStateInterface $form_state ) {
    $this->setConfigurationValue('uc_eway_logo_size', $form_state->getValue('uc_eway_logo_size'));
    $this->setConfigurationValue('uc_eway_logo_colour', $form_state->getValue('uc_eway_logo_colour'));
  }
} # EndOf CLass
