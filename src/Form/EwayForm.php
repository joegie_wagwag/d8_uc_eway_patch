<?php

/**
* @file
* Contains \Drupal\uc_eway\Form\EwayForm.
*/

namespace Drupal\uc_eway\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
* Credit card settings form.
*/
class EwayForm extends ConfigFormBase {

  /**
  * {@inheritdoc}
  */
  public function getFormId() {
    return 'uc_eway_settings_form';
  }

  /**
  * {@inheritdoc}
  */
  public function buildForm( array $form, FormStateInterface $form_state ) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('uc_eway.settings');

    $form['uc_eway'] = array(
      '#type' => 'vertical_tabs',
    );

    // Form elements that deal specifically with card number security.
    $form['eway_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('eWAY Payment Gateway settings'),
    );
    $form['eway_settings']['uc_eway_customer_id'] = array(
      '#type' => 'textfield',
      '#title' => t('eWAY Customer ID'),
      '#default_value' => $config->get('uc_eway_customer_id'),
      '#description' => t('Your eWAY customer ID. Provided to you by eWAY.'),
    );
    $form['eway_settings']['uc_eway_email_address'] = array(
      '#type' => 'textfield',
      '#title' => t('eWAY email login'),
      '#default_value' => $config->get('uc_eway_email_address'),
      '#description' => t('The email address you use to login to eWAY.'),
    );
    $form['eway_settings']['uc_eway_password'] = array(
      '#type' => 'textfield',
      '#title' => t('eWAY password'),
      '#default_value' => $config->get('uc_eway_password'),
      '#description' => t('The password you use to login to eWAY.'),
    );

    $form['eway_settings']['uc_eway_mode'] = array(
      '#type' => 'select',
      '#title' => t('eWAY mode'),
      '#description' => t('eWAY mode used for processing orders. Options are detailed <a href="@url">here</a>.', array('@url' => 'http://www.eway.com.au/Developer/LinkingtoeWAY/overview.aspx')),
      '#options' => array(
        'merchant_xml' => t('Merchant Hosted (XML)'),
        'cvn_xml' => t('CVN (XML)'),
        'beagle' => t('Beagle Anti-Fraud'),
      ),
      '#default_value' => $config->get('uc_eway_mode'),
    );

    $form['eway_settings']['uc_eway_change_order_status'] = array(
      '#type' => 'select',
      '#title' => t('Change order status to "Payment Received" upon successful transaction?'),
      '#description' => t('Change the status of the order if the payment is successful?'),
      '#options' => array(
        1 => t('Yes'),
        0 => t('No'),
      ),
      '#default_value' => $config->get('uc_eway_change_order_status'),
    );

    $form['eway_settings']['uc_eway_show_fail_message'] = array(
      '#type' => 'checkbox',
      '#title' => t("Show gateway's response message on transaction fail."),
      '#description' => t('Systems such as ubercart will show a friendly transaction failed message and may not require an additional message.'),
      '#default_value' => $config->get('uc_eway_show_fail_message'),
    );

    $form['eway_settings']['uc_eway_logo'] = array(
      '#type' => 'checkbox',
      '#title' => t('Display eWAY Logo?'),
      '#description' => t('Displays the eWay logo when processing credit cards. This may be required for some people depending on your terms with eWAY.'),
      '#default_value' => $config->get('uc_eway_logo'),
    );

    // NuSOAP library status.
    $status = file_exists(_uc_eway_nusoap_path());
    $nusoap_msg = $status ? t('NuSOAP library found.') : t('NuSOAP library not found. Make sure that the <a href="@libraries_url">Libraries API module</a> is installed and the <a href="@nusoap_url">NuSOAP library</a> is installed in %libraries_dir', array( '@libraries_url' => 'http://drupal.org/project/libraries', '@nusoap_url' => 'http://sourceforge.net/projects/nusoap/', '%libraries_dir' => 'sites/all/libraries/nusoap') );
    $form['eway_recurring'] = array(
      '#type' => 'fieldset',
      '#title' => t('Recurring Billing settings'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['eway_recurring']['uc_eway_recurring_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable recurring billing support'),
      '#description' => t('Enables support for recurring payments via <a href="@url">eWAY Token Payments</a> <br/> Note that this requires the NuSOAP library to be correctly installed and configured. See README.txt in the uc_eway module directory for instructions.' , array('@url' => 'http://www.eway.com.au/Developer/eway-api/token-payments.aspx')),
      '#default_value' => $config->get('uc_eway_recurring_enabled'),
      '#disabled' => ( !$status && !$config->get('uc_eway_recurring_enabled') ),
    );
    $form['eway_recurring']['uc_eway_recurring_nusoap_status'] = array(
      '#type' => 'markup',
      '#markup' => $nusoap_msg,
      '#prefix' => '<div>',
      '#suffix' => '</div>',
    );

    $form['eway_testing'] = array(
      '#type' => 'fieldset',
      '#title' => t('eWAY Testing settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['eway_testing']['uc_eway_test_mode'] = array(
      '#type' => 'checkbox',
      '#title' => t('Put the eWAY payment gateway into test mode?'),
      '#description' => t('When in testing mode, transactions are not processed. The testing eWAY customer ID (87654321) and testing Credit Card Number (4444333322221111) are used for all transactions, overriding the existing settings. Please note there is currently no test service for the %beagle eWAY mode.', array('%beagle' => 'Beagle Anti-Fraud')),
      '#default_value' => $config->get('uc_eway_test_mode'),
    );
    $form['eway_testing']['uc_eway_test_mode_display_msg'] = array(
      '#type' => 'checkbox',
      '#title' => t('Display a message to the user when in test mode?'),
      '#description' => t('If checked, when a payment is sent to eWAY in testing mode a message will be displayed to the user to tell them that it is in test mode.'),
      '#default_value' => $config->get('uc_eway_test_mode_display_msg'),
    );

    $form['eway_testing']['uc_eway_test_approve_anyway'] = array(
      '#type' => 'checkbox',
      '#title' => t('When in test mode, approve "failed" transactions anyway?'),
      '#description' => t('When in testing mode, a successful transaction will still return a fail. This overcomes that.'),
      '#default_value' => $config->get('uc_eway_test_approve_anyway'),
    );

    $form['eway_watchdog'] = array(
      '#type' => 'fieldset',
      '#title' => t('eWAY Watchdog Logging'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $form['eway_watchdog']['uc_eway_watchdog_status'] = array(
      '#type' => 'select',
      '#title' => t('Watchdog status'),
      '#options' => array(
        UC_EWAY_WATCHDOG_STATUS_OFF => t('Never log messages'),
        UC_EWAY_WATCHDOG_STATUS_ALL => t('Always log messages'),
        UC_EWAY_WATCHDOG_STATUS_TEST => t('Only log messages when in test mode'),
      ),
      '#description' => t('Specify when to log messages to the watchdog log.'),
      '#default_value' => $config->get('uc_eway_watchdog_status'),
    );

    return $form;
  }

  /**
  * {@inheritdoc}
  */
  public function validateForm( array &$form, FormStateInterface $form_state ) {

    $form_state_values = $form_state->getValues();

    if ( $form_state_values['uc_eway_mode'] == 'beagle' && $form_state_values['uc_eway_test_mode'] == 1) {
      $form_state->setError('uc_eway_test_mode', t('The eWAY mode %beagle is currently incompatible with testing.', array('%beagle' => 'Beagle Anti-Fraud')) );
    }

    parent::validateForm($form, $form_state);
  }

  /**
  * {@inheritdoc}
  */
  public function submitForm( array &$form, FormStateInterface $form_state ) {
    parent::submitForm($form, $form_state);

    \Drupal::configFactory()->getEditable('uc_eway.settings')
      ->set('uc_eway_customer_id', $form_state->getValue('uc_eway_customer_id'))
      ->set('uc_eway_email_address', $form_state->getValue('uc_eway_email_address'))
      ->set('uc_eway_password', $form_state->getValue('uc_eway_password'))
      ->set('uc_eway_mode', $form_state->getValue('uc_eway_mode'))
      ->set('uc_eway_change_order_status', $form_state->getValue('uc_eway_change_order_status'))
      ->set('uc_eway_show_fail_message', $form_state->getValue('uc_eway_show_fail_message'))
      ->set('uc_eway_logo', $form_state->getValue('uc_eway_logo'))
      ->set('uc_eway_recurring_enabled', $form_state->getValue('uc_eway_recurring_enabled'))
      ->set('uc_eway_test_mode', $form_state->getValue('uc_eway_test_mode'))
      ->set('uc_eway_test_mode_display_msg', $form_state->getValue('uc_eway_test_mode_display_msg'))
      ->set('uc_eway_test_approve_anyway', $form_state->getValue('uc_eway_test_approve_anyway'))
      ->set('uc_eway_watchdog_status', $form_state->getValue('uc_eway_watchdog_status'))
      ->save();
  }

  /**
  * {@inheritdoc}
  */
  protected function getEditableConfigNames() {
    return [
      'uc_eway.settings',
    ];
  }


  /**
  * Returns the path of the NuSOAP library.
  */
  function _uc_eway_nusoap_path() {
    if (function_exists('libraries_get_path')) {
      return libraries_get_path('nusoap') . '/lib/nusoap.php';
    }
    return '';
  }

}
