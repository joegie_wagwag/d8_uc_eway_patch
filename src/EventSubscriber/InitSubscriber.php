<?php /**
 * @file
 * Contains \Drupal\uc_eway\EventSubscriber\InitSubscriber.
 */

namespace Drupal\uc_eway\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class InitSubscriber implements EventSubscriberInterface {

  public function onEvent() {
    // @FIXME
    // Could not extract the default value because it is either indeterminate, or
    // not scalar. You'll need to provide a default value in
    // config/install/uc_eway.settings.yml and config/schema/uc_eway.schema.yml.
    if (\Drupal::config('uc_eway.settings')->get('uc_eway_recurring_enabled')) {
      require_once('uc_eway.recurring.inc');
    }
  }
  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [KernelEvents::REQUEST => ['onEvent', 0]];
  }

}
